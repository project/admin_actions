# Synopsis

This features exposes buttons for performing single actions directly upon an entity,
eg, 'promote to front page' or 'unpublish'. It presents them as a one-click action right on the entity view page, not
through the admin back-end content management UI.

Although the basic actions from core (as seen on the `/admin/content` "Action" drop-down) are supported OOTB, This way
of managing actions is most useful for non-core actions and custom actions, such as
'check for broken links', 'convert to page',
or 'set expiry date 3 months into the future'.

This UI does not supply the actions themselves, just shifts them from
Views Bulk Operations module (VBO) into the entity view (or edit) page directly for easier access.
The available actions themselves are from Drupal core, core `actions` module, and contrib modules
like [views_bulk_edit](https://www.drupal.org/project/views_bulk_edit) (VBO currently does not have its own 'update
field' action).

# Installation

This feature provides a *block* that is to be displayed on the page alongside
entity pages, such as view pages or edit forms, such as `/node/*`.

On installation, the module will try to enable and add this block to this page pattern in a default location. You should
probably update this block position to match your theme.
`/admin/structure/block/manage/views_block__admin_actions_admin_actions_block`

When enabled, a few example core actions (promote, unpublish) are already on the
utility to show it working.
You probably won't want just them, so it's OK to remove them and add your own.
Core already supports the basic actions in many ways, so exposing core actions is of limited extra utility. This module
is most useful when enabling custom actions, such as from contrib modules.
You can do things that support your business rules like: "Mark as reviewed" which may mean "Set next review date 3
months into the future, and set myself as last reviewer".

Choose your desired actions, and their labels, from the available list on the Views edit form
at `/admin/structure/views/view/admin_actions` (requires Views UI) by editing
the `Fields: Global: Views bulk operations` widget.

Although one natural place to perform admin actions is on node edit forms,
you can also use the block positioning rules to have it show up on node
full-view pages, so you can 'promote to front page' or 'unpublish' an item
directly from the front-end.
Access to these buttons will be handled by user role permissions set on the
view, so you would not expect to allow anonymous users to see these tasks.

# Adding actions

You are expected to edit the view yourself at
`admin/structure/views/view/admin_actions`
Under `Fields`, select `Global: Views bulk operations`
and select from the available actions.
More actions can become available from other modules that implement
`hook_action_info()`
Local actions can also be configured at `/admin/config/system/actions`

You should probably select 'skip confirmation step' when adding actions.

If your action needs additional input, eg "change author" action will need to know which author to change it to, then
this phase will be supported also.

# Extending control

## Per Entity Type

The initial setup provides one set of VBO buttons and makes it available
for all node types.

### Restrict to certain nodes

Is easiest done by just changing the "Content Types" restriction at
`/admin/structure/block/manage/views_block__admin_actions_admin_actions_block`

### Different buttons for different content types

For more advanced control, you may want to have different button sets in different places or for different entity types.
You can *duplicate* the block within the Views UI
`/admin/structure/views/view/admin_actions/edit/admin_actions_block`
and adjust the allowed "Bulk Operations" buttons on that block to create a new set of actions.
Place this cloned block nearby your target content type, and customize from there.

> Any view that either has `admin_actions` as the `view->id` (any block that extends the default provided view) OR any
> view that has the `Administrative tag` set to `admin_actions` (you can create your own unique sets and tag them) will be
> caught and can be used as an admin_actions view.

## Permissions

When installed, this utility is restricted to users with
`Permission | Administer content`
This is a reasonably high-level permission, and not suitable for daily
content management. You can adjust this permission to your needs by editing the
view, or cloning the block into another display with different buttons and
different access controls.

More granular control is available to actions if you enable
**"Actions permissions (part of VBO)"**.

# Implementation

### Limitations

The actions cannot be inserted into the edit form itself directly
(eg, in a vertical tab)
as HTML does not allow nested forms, and the form being used here
(from Views Bulk Operations) is its own thing.
Ideally this utility may be better integrated via some other method.
The 'block' method is just a low-impact way of leveraging
existing VBO functionality and already-existing
Drupal site building conventions.

### Drupal-native site-building methodology

How this works internally.

Most of the functionality described is just leveraging things that VBO
and core already provide you. You could have achieved this result yourself with VBO only, and a little theming.
This module just provides you with a starting point for your own modifications.

This module does not handle the processing of the actions at all, it's just a wrapper that hands it off to the same
execution pathway as the 'actions' you would use on the bulk content management form. If you encounter unexpected
difficulties, compare the behaviour with what happens if you were to visit `/admin/content`, select a single node, and a
single `Action` from the actions list, and execute. That is what this module is doing for you, with a restructured UI.

This module is intentionally minimal code, leveraging VBO+Views 99% without
adding new code. As such, it's a bit clunky in some ways,
but should feel familiar in other ways.
To further refine the behaviour, we'd need to re-imagine the task and build
it our own way.

After auto-configuring the provided view block, this module only provides a
small amount of form-formatting help to hide some VBO defaults. (It visually hides the checkbox, and auto-selects it,
leaving only thye VBO buttons visible.)
