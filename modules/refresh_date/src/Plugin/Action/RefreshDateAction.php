<?php

namespace Drupal\refresh_date\Plugin\Action;

use Drupal;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * An example action to perform a simple node update action.
 *
 * No options or configuration or confirmation. For more on that,
 * look into actions API,
 * - for preconfigured global actions,
 * or ViewsBulkOperationExampleAction
 * - which will allow configuration right inside the Views UI .
 *
 * This action should be detected as an available action because VBO scans all
 * modules to a thing using the PSR namespace ..\Plugin\Action.
 * Not due to the fact that it isa(Action).
 * It just iterates directories doing a 'discovery' for filepaths.
 *
 * @see AnnotatedClassDiscovery::getDefinitions()
 *
 * VBO ViewsBulkOperationsActionManager::findDefinitions()
 * will discard my actions unless they are either derived from
 *   ViewsBulkOperationsActionBase OR have a defined `type`. Cannot provide
 *   actions with no `type` or VBO admin form will not list them.
 *
 * @Action(
 *   id = "node_refresh_date_action",
 *   label = @Translation("admin_actions example action : updates node date"),
 *   type = "node",
 *   confirm = FALSE,
 *   api_version = "1",
 * )
 */
class RefreshDateAction extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    /*
     * All config resides in $this->configuration.
     * Passed view rows will be available in $this->context.
     * Data about the view used to select results and optionally
     * the batch context are available in $this->context or externally
     * through the public getContext() method.
     * The entire ViewExecutable object  with selected result
     * rows is available in $this->view or externally through
     * the public getView() method.
     */
    // Normally a node gets 'changed' updated often automatically,
    // But it's the `created` date that gets visually displayed usually.
    // As we want to demonstrate some actual action, we will refresh
    // the `created` date here, so you can see something new happening.
    $entity->set('created', Drupal::time()->getRequestTime());
    $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    // @see Drupal\Core\Field\FieldUpdateActionBase::access().
    return $object->access('update', $account, $return_as_object);
  }

}
